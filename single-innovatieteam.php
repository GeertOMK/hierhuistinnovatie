<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package hierhuistinnovatie
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

	<?php
	while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/content', get_post_type() );

		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

	endwhile; // End of the loop.
	?>

	</main><!-- #main -->
</div><!-- #primary -->


<div class="verhalen-block-container">
	<div class="verhalen-block">
		<article class="verhalen cta-hulplijn">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-huis-geel.svg" /><br/>
			<span class="verhalen-block-item--content-title">Hé! Heb jij het huis in handen? Bekijk hier de gebruiksaanwijzing!</span>
			<a href="https://hierhuistinnovatie.nl/gebruiksaanwijzing-van-het-huis/" class="button-yellow">Gebruiksaanwijzing<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/button-arrow-right-grey.png" /></a>
		</article>
		<?php 
		// Set the arguments for the query
		$args = array( 
			'post_type'		=> 'innovatieteam', // or 'post', 'page'
			'orderby' 		=> 'date', // or 'date', 'rand'
			'order' 		=> 'DESC', // or 'DESC'
		);

		$innovatieteam = new WP_Query( $args );
 		if ( $innovatieteam->have_posts() ) {
	   	while ( $innovatieteam->have_posts() ) : $innovatieteam->the_post();
	   		
				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/innovatieteam', 'overzicht-item' );

		endwhile;
		}
			wp_reset_postdata();
		?>
	</div>
</div>

<?php
get_footer();
