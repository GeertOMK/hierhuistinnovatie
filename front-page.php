<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header(); ?>
<div class="map-container">
	<div class="website-title">
		<h1>Wij ontketenen de innovatieve slagkracht van Drachten & Smallingerland</h1>
		<span>Klik op een lokatie om ons te bezoeken:</span>
	</div>
	<?php echo do_shortcode("[put_wpgm id=2]"); ?>
</div>
<div style="clear: both;"></div>

<div class="waarom-block-container">
	<div class="container-inner">
		<a class="arrow-down" href="#waarom-hhi">
		</a>
		<div class="intro-block" id="waarom-hhi">
			<span class="intro-sub-title">Samen heel praktisch aan de slag met vernieuwing?</span>
			<h2 class="intro-title">Typisch #hierhuistinnovatie!</h2>
			<p>Het tempo waarin de wereld om ons heen verandert is hoger dan ooit. Om samen de vraag om vernieuwing te kunnen beantwoorden, zullen we ook de manieren waarop we samenwerken moeten vernieuwen. Binnen #hierhuistinnovatie slaan Overheid, Onderwijs en Ondernemend Drachten en Smallingerland de handen ineen. Alleen samen komen we tot antwoorden op vragen van vandaag en morgen. Zet jij jezelf op de kaart en beweeg je met ons mee?</p>
		</div>
		<div class="waarom-content">
			<div class="waarom-content-item item-one">
				<div class="cirkel-number"></div>
				<div class="inner">
					<strong>Heel praktisch innoveren met de O3 in Drachten en Smallingerland?</strong> Dat begint bij jou! Met een simpele aanmelding sluit ook jij je aan bij onze beweging. 
				</div>
			</div>
			<div class="waarom-content-item item-two">
				<div class="cirkel-number"></div>
				<div class="inner">
					<strong>Heb je je aangemeld?</strong> Dan komt het InnovatieHuis straks ook jouw kant op! In het huis geef je aan waar je om zit te springen om te kunnen vernieuwen en welke kennis en kunde je te bieden hebt.
				</div>
			</div>
			<div class="waarom-content-item item-three">
				<div class="cirkel-number"></div>
				<div class="inner">
					<strong>De volgende stap?</strong> Samen aan de slag! Ons Innovatieteam staat te popelen om samen met jullie te komen tot vernieuwingen waar we allemaal beter van worden…
				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_template_part( 'template-parts/verhalen', 'overzicht' );

get_footer(); ?>