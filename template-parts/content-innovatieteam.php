<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hierhuistinnovatie
 */

?>

<?php 
$image = get_field('hulplijn_header_afbeelding');
$size = 'verhalen-header';
$thumb = $image['sizes'][ $size ];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" <?php if( get_field('hulplijn_header_afbeelding') ): ?>style="background-image: url('<?php echo esc_url($thumb); ?>');"<?php endif; ?>>
		<div class="verhalen-block-item--gradient">
			</div>
		<div class="verhalen-header-content">
			
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="verhalen-header-smalltext">
				<?php
					the_field('hulplijn_functie_van_deze_persoon'); echo ' - Innovatieteam'; 
			 	?>
		 	</div>
		</div>
	</header><!-- .entry-header -->
	<div class="header-content">
		<div class="header-content-intro">
			<?php the_field('hulplijn_samenvatting'); ?>
		</div>
		<a class="arrow-down" href="#entry"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/white-arrow-down.svg" /></a>
	</div>
	<div class="entry-content" id="entry">
		<?php
		the_content();
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php 
		$prev_post = get_adjacent_post(false, '', true);
		$next_post = get_adjacent_post(false, '', false);?>

		<?php 
		if(!empty($prev_post)) {
		echo '<div class="prev-post"><div class="prev-post-inner"><a href="' . get_permalink($prev_post->ID) . '" title="' . $prev_post->post_title . '">Bekijk het vorige teamlid<br/><strong>' . $prev_post->post_title . '</strong></a></div></div>'; }
		?>
		
		<?php
		if(!empty($next_post)) {
		echo '<div class="next-post"><div class="next-post-inner"><a href="' . get_permalink($next_post->ID) . '" title="' . $next_post->post_title . '">Bekijk het volgende teamlid<br/><strong>' . $next_post->post_title . '</strong></a></div></div>'; }
		?>

	</footer>
</article><!-- #post-<?php the_ID(); ?> -->