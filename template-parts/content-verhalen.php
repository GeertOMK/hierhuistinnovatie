<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hierhuistinnovatie
 */

?>

<?php 
$image = get_field('header_afbeelding_verhaal');
$size = 'verhalen-header';
$thumb = $image['sizes'][ $size ];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" <?php if( get_field('header_afbeelding_verhaal') ): ?>style="background-image: url('<?php echo esc_url($thumb); ?>');"<?php endif; ?>>
		<div class="verhalen-block-item--gradient">
			</div>
		<div class="verhalen-header-content">
			
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="verhalen-header-smalltext">
				<?php
					the_field('functie_van_deze_persoon'); echo ' - '; 
					$terms = wp_get_post_terms($post->ID, 'category');
						$count = count($terms);
						if ( $count > 0 ) {
						    foreach ( $terms as $term ) {
						        echo $term->name;
						    }
						}
			 	?>
		 	</div>
		</div>
	</header><!-- .entry-header -->
	<div class="header-content">
		<div class="elipse-number"><div class="number"><?php the_field('huis_nummer'); ?></div></div>
		<div class="content-meta">
			<ul class="content-meta-list">
				<li>Huis ontvangen op <?php the_field('huis_ontvangen_op'); ?> van <?php the_field('huis_ontvangen_van'); ?></li>
				<li>Huis doorgegeven op <?php the_field('huis_doorgegeven_op'); ?> aan <?php the_field('huis_doorgegeven_aan'); ?></li>
				<li>Laatste update verhaal: <?php printf(get_the_modified_date() ); ?></li>
			</ul>
		</div>
		<div class="header-content-intro">
			<?php the_field('verhaal_samenvatting'); ?>
		</div>
		<a class="arrow-down" href="#entry"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/white-arrow-down.svg" /></a>
	</div>
	<div class="entry-content" id="entry">
		<?php
		the_content();
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php 
		$prev_post = get_adjacent_post(false, '', true);
		$next_post = get_adjacent_post(false, '', false);?>

		<?php 
		if(!empty($prev_post)) {
		echo '<div class="prev-post"><div class="prev-post-inner"><a href="' . get_permalink($prev_post->ID) . '" title="' . $prev_post->post_title . '">Bekijk de vorige innovatiehuis eigenaar<br/><strong>' . $prev_post->post_title . '</strong></a></div></div>'; }
		?>
		
		<?php
		if(!empty($next_post)) {
		echo '<div class="next-post"><div class="next-post-inner"><a href="' . get_permalink($next_post->ID) . '" title="' . $next_post->post_title . '">Bekijk de volgende innovatiehuis eigenaar<br/><strong>' . $next_post->post_title . '</strong></a></div></div>'; }
		?>

	</footer>
</article><!-- #post-<?php the_ID(); ?> -->
