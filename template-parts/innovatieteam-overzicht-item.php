<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hierhuistinnovatie
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    $image = get_field('hulplijn_overzicht_afbeelding');
    $size = 'verhalen-overzicht';
    $thumb = $image['sizes'][ $size ];
    ?>
    <a href="<?php echo esc_url( get_permalink() ); ?>">
        <div class="verhalen-block-item--image" <?php if( get_field('hulplijn_overzicht_afbeelding') ): ?>style="background-image: url('<?php echo esc_url($thumb); ?>');"<?php endif; ?>>
        </div>
        <div class="verhalen-block-item--gradient">
        </div>
        <div class="verhalen-block-item--content">
            <?php 
            the_title( '<span class="verhalen-block-item--content-title">', '</span>' ); 
            ?>
            <span class="smalltext">
                <?php
                    the_field('hulplijn_functie_van_deze_persoon'); echo ' - Innovatieteam'; 
                ?>
            </span>
        </div>
        <div class="verhalen-block-item--content-button"></div>
    </a>
</article><!-- #post-<?php the_ID(); ?> -->
