<?php
/**
 * Template part for displaying stories in a block grid
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hierhuistinnovatie
 */

?>


<div class="verhalen-block-container">
	<a class="arrow-down" href="#verhalen_block">
	</a>
	<div class="intro-block" id="verhalen_block">
		<span class="intro-sub-title">Initaitiefnemers, ondernemer of een hele andere rol…</span>
		<h2 class="intro-title">Met deze mensen zijn we nu al bezig!</h2>
	</div>
	<div class="verhalen-block">
		<?php 
		// Set the arguments for the query
		$args = array( 
			'post_type'		=> 'verhalen', // or 'post', 'page'
			'orderby' 		=> 'date', // or 'date', 'rand'
			'order' 		=> 'DESC', // or 'DESC'
		);

		$verhalen = new WP_Query( $args );
 		if ( $verhalen->have_posts() ) {
	   	while ( $verhalen->have_posts() ) : $verhalen->the_post();
	   		
				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/verhalen', 'overzicht-item' );

		endwhile;
		}
			wp_reset_postdata();
		?>
		<article class="verhalen cta-verhaal">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-huis-geel.svg" /><br/>
			<span class="verhalen-block-item--content-title">Hé! Sta jij hier nog niet tussen? Meld je aan en beweeg met ons mee!</span>
			<a href="#cta" class="button-yellow">Ik wil me aanmelden <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/button-arrow-right-grey.png" /></a>
		</article>

		<article class="verhalen cta-hulplijn">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-huis-blauw.svg" /><br/>
					<span class="verhalen-block-item--content-title">Heb je het InnovatieHuis in handen? Hier ontdek je wat we van je vragen!</span>
					<a href="https://hierhuistinnovatie.nl/ID-Gebruiksaanwijzing.pdf" target="_blank" class="button-yellow">Download de gebruiksaanwijzing<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/button-arrow-right.png" /></a>
				</article>
	</div>
</div>