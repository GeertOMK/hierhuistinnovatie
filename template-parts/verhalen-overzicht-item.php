<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hierhuistinnovatie
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    $image = get_field('overzicht_afbeelding_verhaal');
    $size = 'verhalen-overzicht';
    $thumb = $image['sizes'][ $size ];
    ?>
    <a href="<?php echo esc_url( get_permalink() ); ?>">
        <div class="verhalen-block-item--image" <?php if( get_field('overzicht_afbeelding_verhaal') ): ?>style="background-image: url('<?php echo esc_url($thumb); ?>');"<?php endif; ?>>
        </div>
        <div class="verhalen-block-item--gradient">
        </div>
        <div class="verhalen-block-item--content">
            <?php 
            the_title( '<span class="verhalen-block-item--content-title">', '</span>' ); 
            ?>
            <span class="smalltext">
                <?php
                    the_field('functie_van_deze_persoon'); echo ' - '; 
                    $terms = wp_get_post_terms($post->ID, 'category');
                        $count = count($terms);
                        if ( $count > 0 ) {
                            foreach ( $terms as $term ) {
                                echo $term->name;
                            }
                        }
                ?>
            </span>
        </div>
        <div class="verhalen-block-item--content-button"></div>
    </a>
</article><!-- #post-<?php the_ID(); ?> -->
