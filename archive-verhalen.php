<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hierhuistinnovatie
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<span class="sub-title">Initaitiefnemers, ondernemer of een hele andere rol…</span>
				<h1 class="page-title">Met deze mensen zijn we nu al bezig!</h1>
			</header><!-- .page-header -->
			<div class="verhalen-block">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					 * Include the Post-Type-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
					 */
					get_template_part( 'template-parts/verhalen', 'overzicht-item' );

				endwhile;
				?>
				<article class="verhalen cta-verhaal">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-huis-geel.svg" /><br/>
					<span class="verhalen-block-item--content-title">Hé! Sta jij hier nog niet tussen? Meld je aan en beweeg met ons mee!</span>
					<a href="#cta" class="button-yellow">Ik wil me aanmelden <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/button-arrow-right-grey.png" /></a>
				</article>

				<article class="verhalen cta-hulplijn">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-huis-blauw.svg" /><br/>
					<span class="verhalen-block-item--content-title">Heb je het InnovatieHuis in handen? Hier ontdek je wat we van je vragen!</span>
					<a href="https://hierhuistinnovatie.nl/ID-Gebruiksaanwijzing.pdf" target="_blank" class="button-yellow">Download de gebruiksaanwijzing<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/button-arrow-right.png" /></a>
				</article>
			</div>
			<?php
			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
