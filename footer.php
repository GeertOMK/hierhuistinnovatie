<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hierhuistinnovatie
 */

?>
<script type="text/javascript">
	jQuery(function() {
  jQuery('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    jQuery('html, body').animate({ scrollTop: jQuery(jQuery(this).attr('href')).offset().top}, 500, 'linear');
  });
});
</script>
	</div><!-- #content -->
	<div class="call-to-action">
		<a class="arrow-down" href="#cta">
		</a>
			<div class="intro-block" id="cta">
				<span class="intro-sub-title">Een groeiend innovatief netwerk..</span>
				<h3 class="intro-title">Ben jij onze volgende uitbreiding? Je bent welkom!</h3>
			</div>
			<div class="form-block">
				<div class="form-block-content">
					<div class="form-block-left">
						<span>Zet jij jezelf op de kaart?<br/>Beweeg met ons mee op #hierhuistinnovatie</span>
					</div>
					<div class="form-block-right">
						<?php echo do_shortcode('[contact-form-7 id="77" title="Call to action"]');?>
					</div>
				</div>
				<div class="form-block-logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hierhuistinnovatie-logo.svg" />
				</div>
			</div>
	</div>
	<footer class="site-footer">
		<div class="site-footer-contact">
			<div class="container-inner">
				<div class="site-footer-contact-item">
					Bellen met ons<br/>
					<span class="strong-content">Tom: 06-54928091</span><br/><span class="strong-content">Sandra: 06-11351753</span>
				</div>
				<div class="site-footer-contact-item">
					Mail met ons<br/>
					<span class="strong-content"><a href="mailto:Info@hierhuistinnovatie.nl">Info@hierhuistinnovatie.nl</a></span>
				</div>
				<div class="site-footer-contact-item">
					Volg #hierhuistinnovatie op:<br/>
					<span class="strong-content"><a href="https://www.instagram.com/hierhuistinnovatie/?hl=nl" target="_blank">Instagram</a>, <a href="https://www.facebook.com/hierhuistinnovatie/" target="_blank">Facebook</a> & <a href="https://www.linkedin.com/company/hierhuistinnovatie/" target="_blank">LinkedIn</a></span>
				</div>
				<div class="site-footer-contact-item">
					Mede mogelijk gemaakt door:<br/>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-provinsje-fryslan.svg" />
				</div>
			</div>
		</div>
		<div class="site-footer-disclaimer">
			<div class="container-inner">
				<div>© #hierhuistinnovatie </div><?php
				wp_nav_menu( array(
					'theme_location' => 'menu-2',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
